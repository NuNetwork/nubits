# Nu Network Reference Wallet

## NuBits and NuShares for Exchanges

Compile NuBits Wallet and read <https://nubits.com/list-nubits>.

## NuBits and NuShares for Individuals

Download NuBits Wallet for macOS, Windows, and Linux at <https://nubits.com/wallet>.